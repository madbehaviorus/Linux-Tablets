#!/bin/bash

# rotate script for Goodix Capacitive TouchScreen (pointer)
# Medion E1002

if echo "$1" | grep -iq "^0" ;then
        # Currently top is rotated left, we should set it normal (0°) richtig
        xrandr -o 0
        xinput set-prop "pointer:Goodix Capacitive TouchScreen" --type=float "Coordinate Transformation Matrix" 1 0 0 0 1 0 0 0 1
        #xfconf-query -c xsettings -p /Xft/RGBA -s rgb
fi
        
if echo "$1" | grep -iq "^3" ;then
        # Screen is not rotated, we should rotate it right (90°) richtig
        xrandr -o 3
        xinput set-prop "pointer:Goodix Capacitive TouchScreen" --type=float "Coordinate Transformation Matrix" 0 1 0 -1 0 1 0 0 1
        #xfconf-query -c xsettings -p /Xft/RGBA -s vbgr
fi
       
if echo "$1" | grep -iq "^2" ;then
        # Top of screen is rotated right, we should invert it (180°) richtig
        xrandr -o 2
        xinput set-prop "pointer:Goodix Capacitive TouchScreen" --type=float "Coordinate Transformation Matrix" -1 0 1 0 -1 1 0 0 1
        #xfconf-query -c xsettings -p /Xft/RGBA -s bgr
fi

if echo "$1" | grep -iq "^1" ;then        
        # Screen is inverted, we should rotate it left (270°) richtig
        xrandr -o 1
        xinput set-prop "pointer:Goodix Capacitive TouchScreen" --type=float "Coordinate Transformation Matrix" 0 1 -1 -1 0 0 0 0 -1
        #xfconf-query -c xsettings -p /Xft/RGBA -s vrgb
fi
 
           

