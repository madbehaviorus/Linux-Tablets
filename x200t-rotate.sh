#!/bin/bash

# rotate script for Wacom Serial Penabled 1FG Touchscreen Pen stylus, Pen eraser, Finger Touch
# x200t
# see https://github.com/linuxwacom/xf86-input-wacom/wiki/xsetwacom

if echo "$1" | grep -iq "^0" ;then
        # Currently top is rotated, we set it to normal (0°) 
        xrandr -o 0
        xsetwacom set "Wacom Serial Penabled 1FG Touchscreen Pen stylus" rotate none
        xsetwacom set "Wacom Serial Penabled 1FG Touchscreen Pen eraser" rotate none
        xsetwacom set "Wacom Serial Penabled 1FG Touchscreen Finger touch" rotate none
        xsetwacom set "Wacom Serial Penabled 2FG Touchscreen Pen stylus" rotate none
        xsetwacom set "Wacom Serial Penabled 2FG Touchscreen Pen eraser" rotate none
        xsetwacom set "Wacom Serial Penabled 2FG Touchscreen Finger touch" rotate none
        xsetwacom set "Wacom ISDv4 E6 Pen stylus" rotate none
        xsetwacom set "Wacom ISDv4 E6 Pen eraser" rotate none
        xsetwacom set "Wacom ISDv4 E6 Finger touch" rotate none
fi
        
if echo "$1" | grep -iq "^3" ;then
        # rotate the screen to right (90°)
        xrandr -o 3
        xsetwacom set "Wacom Serial Penabled 1FG Touchscreen Pen stylus" rotate cw
        xsetwacom set "Wacom Serial Penabled 1FG Touchscreen Pen eraser" rotate cw
        xsetwacom set "Wacom Serial Penabled 1FG Touchscreen Finger touch" rotate cw
        xsetwacom set "Wacom Serial Penabled 2FG Touchscreen Pen stylus" rotate cw
        xsetwacom set "Wacom Serial Penabled 2FG Touchscreen Pen eraser" rotate cw
        xsetwacom set "Wacom Serial Penabled 2FG Touchscreen Finger touch" rotate cw
        xsetwacom set "Wacom ISDv4 E6 Pen stylus" rotate cw
        xsetwacom set "Wacom ISDv4 E6 Pen eraser" rotate cw
        xsetwacom set "Wacom ISDv4 E6 Finger touch" rotate cw

fi
       
if echo "$1" | grep -iq "^2" ;then
        # invert the screen (180°)
        xrandr -o 2
        xsetwacom set "Wacom Serial Penabled 1FG Touchscreen Pen stylus" rotate half
        xsetwacom set "Wacom Serial Penabled 1FG Touchscreen Pen eraser" rotate half
        xsetwacom set "Wacom Serial Penabled 1FG Touchscreen Finger touch" rotate half
        xsetwacom set "Wacom Serial Penabled 2FG Touchscreen Pen stylus" rotate half
        xsetwacom set "Wacom Serial Penabled 2FG Touchscreen Pen eraser" rotate half
        xsetwacom set "Wacom Serial Penabled 2FG Touchscreen Finger touch" rotate half
        xsetwacom set "Wacom ISDv4 E6 Pen stylus" rotate half
        xsetwacom set "Wacom ISDv4 E6 Pen eraser" rotate half
        xsetwacom set "Wacom ISDv4 E6 Finger touch" rotate half
fi

if echo "$1" | grep -iq "^1" ;then        
        # rotate the screen to left (270°) 
        xrandr -o 1
        xsetwacom set "Wacom Serial Penabled 1FG Touchscreen Pen stylus" rotate ccw
        xsetwacom set "Wacom Serial Penabled 1FG Touchscreen Pen eraser" rotate ccw
        xsetwacom set "Wacom Serial Penabled 1FG Touchscreen Finger touch" rotate ccw
        xsetwacom set "Wacom Serial Penabled 2FG Touchscreen Pen stylus" rotate ccw
        xsetwacom set "Wacom Serial Penabled 2FG Touchscreen Pen eraser" rotate ccw
        xsetwacom set "Wacom Serial Penabled 2FG Touchscreen Finger touch" rotate ccw
        xsetwacom set "Wacom ISDv4 E6 Pen stylus" rotate ccw
        xsetwacom set "Wacom ISDv4 E6 Pen eraser" rotate ccw
        xsetwacom set "Wacom ISDv4 E6 Finger touch" rotate ccw
fi
 
           

