
## Medion E2002

#### Install
	* for install Linux to this "Tablet" start the live CD from your ditribution you prefer
	* for *ubuntu install the grub-efi-ia32_2.02-2ubuntu8.17_i386.deb with
	dpkg -i grub-efi-ia32_2.02-2ubuntu8.17_i386.deb
	* and for debian based systems install grub-efi-ia32-bin_2.02+dfsg1-20+deb10u3_amd64.deb
	dpkg -i grub-efi-ia32-bin_2.02+dfsg1-20+deb10u3_amd64.deb
	* now you can install your favorite distribution without the abort by install the grub bootloader
	
	* note: before start the live usb, you must disable the *** "secure" boot
	* note2 : don't forget the tablet have one usb port....,but a sd-card slot also ;-)
	
#### config
	* if you install the distribution with full luks encrypted device (without the boot partition [standard lvm luks install];  FDE not tested yet), you should attention by type the luks password:
	* the keyboard starts in the "Fn-mode"
	* to use the standard keyboard layout, you should type the "Fn" + "Num"
	* now you can type your luks password
	
	* to use the tablet without mouse complications and easy change the rotate with these things, 
	you should use the rotate.sh, for example ./medion-E2002-rotate.sh 3
	* i prefer to start the script with login and use it as alias
	




## Thinkpads
	* Thinkpad x200t (Tablet) - with following wacom articles:
	    - "Wacom Serial Penabled 1FG Touchscreen Pen stylus", "...Pen eraser" and "...Finger touch"
        - "Wacom Serial Penabled 2FG Touchscreen Pen stylus", "...Pen eraser" and "...Finger touch"


#### HowTo
    * to use the tablet without pen and touch complications by rotating the screens, 
	you should use the $device-rotate.sh, for example ./rotate.sh 3
	* i prefer to start the script with three from the four buttons on the LCD/LED below
        - use the Settings under Keyboard buttons
        - set for every button the the command you need
        - f.exp.: 
        - sh /home/$user/x200-rotate.sh 3



## issues
	* if you find issues, tell me =)

